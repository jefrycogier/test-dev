class LoginResponseModel {
  final String token;
  final String message;

  LoginResponseModel({this.token, this.message});

  factory LoginResponseModel.fromJson(Map<String, dynamic> json) {
    return LoginResponseModel(
      token: json["accessToken"] != null ? json["accessToken"] : "",
      message: json["message"] != null ? json["message"] : "",
    );
  }
}

class LoginRequestModel {
  String usr;
  String pwd;

  LoginRequestModel({
    this.usr,
    this.pwd,
  });

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'usr': usr.trim(),
      'pwd': pwd.trim(),
    };

    return map;
  }
}
