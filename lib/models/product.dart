import 'dart:convert';
import 'package:get/get.dart';

List<Product> productFromJson(String str) =>
    List<dynamic>.from(json.decode(str).map((x) => Product.fromJson(x)));

String productToJson(List<Product> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Product {
  Product({
    this.item_code,
    this.item_name,
    this.image,
    this.normal_rate,
    this.promo_rate,
  });

  String item_code;
  String item_name;
  String image;
  double normal_rate;
  double promo_rate;

  var isFavorite = false.obs;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        item_code: json["item_code"],
        item_name: json["item_name"],
        image: json["image"],
        promo_rate: json["promo_rate"],
        normal_rate: json["normal_rate"],
      );

  Map<String, dynamic> toJson() => {
        "item_code": item_code,
        "item_name": item_name,
        "image": image,
        "normal_rate": normal_rate,
        "promo_rate": promo_rate,
      };
}
