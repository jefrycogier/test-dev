import 'package:http/http.dart' as http;
import 'package:shopx/models/product.dart';
import '../services/url.dart';
import 'dart:convert';

class RemoteServices {
  static var client = http.Client();

  static Future<List<Product>> fetchProducts() async {
    final response = await http.get(Uri.parse('$GETLISTALL'));
    print(response.body);

    if (response.statusCode == 200) {
      final parsed = json.decode(response.body)['data'].cast<Map<String, dynamic>>();
      print(parsed);

      return parsed.map<Product>((json) => Product.fromJson(json)).toList();
    } else {
      //show error message
      return null;
    }
  }
}
