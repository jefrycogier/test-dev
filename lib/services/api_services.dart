import 'package:http/http.dart' as http;
import 'dart:convert';
import '../models/login.dart';
import '../services/url.dart';

class APIService {
  Future<LoginResponseModel> login(LoginRequestModel requestModel) async {
    final response = await http.post(Uri.parse('$LOGINAUTH'),body: requestModel.toJson());
    print(response.body);
    print(response.statusCode);
    if (response.statusCode == 200 || response.statusCode == 400 || response.statusCode == 401) {
      return LoginResponseModel.fromJson(
        json.decode(response.body),
      );
    } else {
      throw Exception('Failed to load data!');
    }
  }
}
