import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopx/models/product.dart';
import 'dart:convert';
import 'dart:typed_data';
import 'package:intl/intl.dart';



class ProductTile extends StatelessWidget {
  final Product product;
  const ProductTile(this.product);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Container(
                  height: 180,
                  width: double.infinity,
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: product.image == null
                      ? Image.asset("assets/image/empty_image.png")
                      : Image.memory(base64.decode(product.image.split(',').last))
                ),
                Positioned(
                  right: 0,
                  child: Obx(() => CircleAvatar(
                        backgroundColor: Colors.white,
                        child: IconButton(
                          icon: product.isFavorite.value
                              ? Icon(Icons.favorite_rounded)
                              : Icon(Icons.favorite_border),
                          onPressed: () {
                            product.isFavorite.toggle();
                          },
                        ),
                      )),
                )
              ],
            ),
            SizedBox(height: 8),
            Text(
              product.item_name,
              maxLines: 2,
              style:
                  TextStyle(fontFamily: 'avenir', fontWeight: FontWeight.w800),
              overflow: TextOverflow.ellipsis,
            ),
            SizedBox(height: 8),
            Text('\Rp ${product.normal_rate}', style: TextStyle(decoration: TextDecoration.lineThrough)),
            Text('\Rp ${product.promo_rate}',
                style: TextStyle(fontSize: 20, fontFamily: 'avenir')),
          ],
        ),
      ),
    );
  }
}
